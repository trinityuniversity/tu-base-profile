# Trinity University PatternLab Project

![Trinity Unversity logo](https://cdn2.webdamdb.com/150th_sm_1utG7lsr6eqV.png?1519405555)

This project template provides a starter kit for managing your site
dependencies with [Composer](https://getcomposer.org/).

If you want to know how to use it as replacement for
[Drush Make](https://github.com/drush-ops/drush/blob/8.x/docs/make.md) visit
the [Documentation on drupal.org](https://www.drupal.org/node/2471553).

This is a Composer-based installation of:

![Lightning logo of a bolt of lightning](https://raw.githubusercontent.com/acquia/lightning/8.x-3.x/lightning-logo.png)
![Build Status](https://travis-ci.org/acquia/lightning.svg?branch=8.x-3.x)](https://travis-ci.org/acquia/lightning)

[Acquia Lightning](https://github.com/acquia/lightning) a Drupal distribution using
[DDev](https://ddev.readthedocs.io/en/stable/), and the
[Particle Theme](https://phase2.gitbook.io/frontend/) which includes
[Pattern Lab](https://patternlab.io/) and
[Bootsrap 4](https://getbootstrap.com/).
Welcome to the future!

## Get Started

!!!!! Extremely important to follow these steps !!!!!

After Cloning this repository:

If you on a Mac, update your dependencies, this project requires PHP 7.2, MySQL, Node 10, NPM, Gulp, & Yeoman

Install [Docker](https://www.docker.com/get-started) for your machine.

### Install or Update Homebrew installed packages

Install [Homebrew](https://brew.sh/)

```bash
brew update
brew upgrade
```

### Install DDev

Install [DDev for Mac](https://ddev.readthedocs.io/en/stable/#homebrewlinuxbrew-macoslinux) or
[DDev for Windows](https://ddev.readthedocs.io/en/stable/#installation-or-upgrade-windows)

### Install PHP 7.2, if not installed

```bash
brew install php72
```

### Install Composer, if not installed

Install [Composer](https://getcomposer.org/)

```bash
brew install composer
```

### Install NVM, if not installed

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

### Node 10. Leave off `nvm alias default v10` to prevent setting system wide node version

```bash
nvm install v10 && nvm alias default v10
```

### NPM 6+

```bash
npm install -g npm@latest
```

```bash
brew update
brew upgrade
```

```bash
cd TU-Base-Profile 
composer install --no-dev
ddev config (make sure to choose drupal8 in the prompts)
ddev restore-snapshot currentDB
ddev start
```

### Using PatternLab

To initially compile PatternLab,
Open second terminal and type:

```bash
cd TU-Base-Profile/docroot/themes/custom/Trinity-Plab
npm install
npm run setup
npm run build:drupal
npm start
```

To compile Drupal Theme, Open third terminal:

```bash
npm run dev:drupal
```

In a browser go to <https://lightning-particle.ddev.site/> to view the site.

Login Info:
username: admin
password: Drupal8_@dm1n

### Cloning the Database

Watch this video from [DDev](https://www.youtube.com/watch?v=Ax-HocnXNbc) on the snapshots feature. To use:

If there is content in your current database, type:

```bash
drush sql-drop -y in a terminal window before ddev restore-snapshot <snapshot DB Name>
ddev snapshot --name=<Name the snapshot db>
```

### Using Drush

To run drush commands with DDev:

```bash
ddev . drush <drush command>
```

### Generating a Component

Components have a specific file structure. Instead of making a developer create all required files by hand, we use a Yeoman generator to easily create new component folders by running the following command:

```bash
npm run new
```

Follow the onscreen prompts for the location, included files, and name of the new component.

### Anatomy of a Component

All components require a set of files:

```bash
# ./source/{design-system}/_patterns/01-atoms/button/
.
├── demo                            # Demo implementations; can be removed on deploy to prod
│   ├── index.js                    # Pulls in Twig, YAML, MD inside demo/ so Webpack is aware
│   ├── buttons.md                  # Markdown with extra notes, visible in PL UI
│   ├── buttons.twig                # Demonstrate with a plural name, visible to PL since no underscore
│   └── buttons.yml                 # Data provided to the demo pattern
├── _button.scss                    # Most components require styles, underscore required
├── _button.twig                    # The pure component template, underscore required to hide from PL UI
└── index.js                        # Component entry point
```

Optionally, you can add a __test__ folder to your components base directory (note the two underscores before and after) for Jest unit testing like so:

```bash
# ./source/{design-system}/_patterns/01-atoms/button/
.
├── __test__                        # Jest unit tests. Read automatically during "npm run test:unit"
│   └── button.test.js              # Unit test JS functions. Limited DOM manipulation
├── ...
```

### Updating Lightning & Drupal Core

This project will attempt to keep all of your Drupal Core files up-to-date; the
project [drupal-composer/drupal-scaffold](https://github.com/drupal-composer/drupal-scaffold)
is used to ensure that your scaffold files are updated every time drupal/core is
updated. If you customize any of the "scaffolding" files (commonly .htaccess),
you may need to merge conflicts if any of your modified files are updated in a
new release of Drupal core.

Follow the steps below to update your core files.

1. Run:

    ```bash
    ddev composer require 'drupal/lightning_core:^<latest-version>'
    ddev composer update
    ```

    to update Drupal Core and its dependencies. With `composer require ...` you can download new dependencies to your
    installation.

    ```bash
    cd <some-dir>
    ddev composer require drupal/devel:~<version-number>
    ```

2. Run: `git diff` to determine if any of the scaffolding files have changed.
   Review the files for any changes and restore any customizations to
   `.htaccess` or `robots.txt`.
3. Commit everything all together in a single commit, so `web` will remain in
   sync with the `core` when checking out branches or running `git bisect`.
4. In the event that there are non-trivial conflicts in step 2, you may wish
   to perform these steps on a branch, and use `git merge` to combine the
   updated core files with your customized files. This facilitates the use
   of a [three-way merge tool such as kdiff3](http://www.gitshah.com/2010/12/how-to-setup-kdiff-as-diff-tool-for-git.html). This setup is not necessary if your changes are simple;
   keeping all of your modifications at the beginning or end of the file is a
   good strategy to keep merges easy.

You can customize your installation by creating a [sub-profile which uses
Lightning as its base profile][sub-profile documentation]. Lightning includes a
Drupal Console command (`lightning:subprofile`) which will generate a
sub-profile for you.

#### Installing from exported config

Lightning can be installed from a set of exported configuration (e.g., using the
--existing-config option with `drush site:install`). This method of installation
is fully supported and tested.

## What Lightning Does

Through custom, contrib, and core modules plus configuration, Lightning aims to
target four functional areas:

### Media

The current version of media includes the following functionality:

* A preconfigured Text Format (Rich Text) with CKEditor WYSIWYG.
* A media button (indicated by a star -- for now) within the WYSIWYG that
  launches a custom media widget.
* The ability to place media into the text area and have it fully embedded as it
  will appear in the live entity. The following media types are currently
  supported:
  * Tweets
  * Instagram posts
  * Videos (YouTube and Vimeo supported out of the box)
  * Images
* Drag-and-drop bulk image uploads.
* Image cropping.
* Ability to create new media through the media library (/media/add)
* Ability to embed tweets, Instagrams, and YouTube/Vimeo videos directly into
  CKEditor by pasting the video URL

#### Extending Lightning Media (Contributed Modules)

Drupal community members have contributed several modules which integrate
Lightning Media with additional third-party media services. These modules are
not packaged with Lightning or maintained by Acquia, but they are stable and you
can use them in your Lightning site:

* [Facebook](https://www.drupal.org/project/lightning_media_facebook)
* [Imgur](https://www.drupal.org/project/lightning_media_imgur)
* [Flickr](https://www.drupal.org/project/lightning_media_flickr)
* [500px](https://www.drupal.org/project/lightning_media_d500px)
* [SoundCloud](https://www.drupal.org/project/lightning_media_soundcloud)
* [Tumblr](https://www.drupal.org/project/lightning_media_tumblr)
* [Spotify](https://www.drupal.org/project/lightning_media_spotify)
* [Pinterest](https://www.drupal.org/project/lightning_media_pinterest)  

### Layout

Lightning includes a Landing Page content type which allows editors to create
and place discrete blocks of content in any order and layout they wish using an
intuitive, accessible interface. Lightning also allows site builders to define
default layouts for content types using the same interface - or define multiple
layouts and allow editors to choose which one to use for each post.

### Workflow

Lightning includes tools for building organization-specific content workflows.
Out of the box, Lightning gives you the ability to manage content in one of four
workflow states (draft, needs review, published, and archived). You can create
as many additional states as you like and define transitions between them. It's
also possible to schedule content to be transitioned between states at a
specific future date and time.

### API-First

Lightning ships with several modules which, together, quickly set up Drupal to
deliver data to decoupled applications via a standardized API. By default,
Lightning installs the OpenAPI and JSON:API modules, plus the Simple OAuth
module, as a toolkit for authentication, authorization, and delivery of data
to API consumers. Currently, Lightning includes no default configuration for
any of these modules, because it does not make any assumptions about how the
API data will be consumed, but we might add support for standard use cases as
they present themselves.

If you have PHP's OpenSSL extension enabled, Lightning can automatically create
an asymmetric key pair for use with OAuth.

## Project Roadmap

We publish sprint plans for each patch release. You can find a link to the
current one in [this meta-issue][meta_releases] on Drupal.org.

## Resources

Demonstration videos for each of our user stories can be found [here][demo_videos].

Please use the [Drupal.org issue queue][issue_queue] for latest information and
to request features or bug fixes.

## Known Issues

### Media Handling

* If you upload an image into an image field using the new image browser, you
  can set the image's alt text at upload time, but that text will not be
  replicated to the image field. This is due to a limitation of Entity Browser's
  API.
* Some of the Lightning contributed media modules listed above might not yet be
  compatible with the Core Media entity.
* Using the bulk upload feature in environments with a load balancer might
  result in some images not being saved.

### Inherited profiles

Drush is not aware of the concept of inherited profiles and as a result, you
will be unable to uninstall dependencies of any parent profile using Drush. You
can still uninstall these dependencies via the UI at "/admin/modules/uninstall".
We have provided patches [here](https://www.drupal.org/node/2902643)
for Drush which allow you to uninstall dependencies of parent profiles.

* [Drush 9 inherited profile dependencies patch](https://www.drupal.org/files/issues/2902643-2--drush-master.patch).

## Contributing

Issues are tracked on [drupal.org][issue_queue]. Contributions can be provided
either as traditional patches or as pull requests on our [GitHub clone][github].

Each Lightning component also has a drupal.org issue queue:

* [API](https://www.drupal.org/project/issues/lightning_api)
* [Core](https://www.drupal.org/project/issues/lightning_core)
* [Layout](https://www.drupal.org/project/issues/lightning_layout)
* [Media](https://www.drupal.org/project/issues/lightning_media)
* [Workflow](https://www.drupal.org/project/issues/lightning_workflow)

For more information on local development, see CONTRIBUTING.md.

### How to uninstall Lightning

Lightning is an installation profile, so there's no "officially" sanctioned way
to remove it. The procedure outlined here is one that you do **at your own
risk**, and in the worst case scenario **it has the potential to break your
site**, so proceed with caution! It _highly_ recommended to first attempt this
in a development environment before doing it in production.

That said:

1. Change the current installation profile from Lightning to Standard, or
   another installation profile of your choice: `drush config:set core.extension profile standard`.
2. Uninstall any Lightning modules you are not actively using.
3. If needed, export your configuration to account for changes made by outgoing
   modules.
4. You may be using modules that ship with Lightning -- ensure that all of them
   are explicitly listed as requirements in your project's composer.json file,
   in at least the same version you were already using. For example, if you are
   using Panelizer 4.2 or later, you should run `composer require --no-update drupal/panelizer:^4.2`.
   This is **very important**, because your site is likely to break if you
   remove Lightning before ensuring that all the modules you need are being
   required by Composer. If you need to continue using a particular Lightning
   module, you can require it just as you would any other Drupal module; for
   example, `composer require --no-update drupal/lightning_api:^4.1`. (Note
   that this "à la carte" style only works with Lightning 3 or later.)
5. Remove `acquia/lightning` from your composer.json file: `composer remove --no-update acquia/lightning`.
6. Explicitly require Drupal core in your composer.json file: `composer require --no-update drupal/core:~8.7.0`.
   You should specify the same minor version of core that you were using when
   Lightning was installed.
7. Run `composer update`.
8. You should now be all set. If you ever reinstall the site, you will need to
   specify the installation profile (probably Standard), either in the web
   installer, `drush site:install PROFILENAME`, or as a configuration parameter
   for BLT.

[issue_queue]: https://www.drupal.org/project/issues/lightning "Lightning Issue Queue"
[meta_release]: https://www.drupal.org/node/2670686 "Lightning Meta Releases Issue"
[template]: https://github.com/acquia/lightning-project "Composer-based project template"
[d.o_semver]: https://www.drupal.org/node/1612910
[lightning_composer_project]: https://github.com/acquia/lightning-project
[demo_videos]: http://lightning.acquia.com/blog/lightning-user-stories-demonstrations "Lightning user story demonstration videos"
[sub-profile documentation]: https://github.com/acquia/lightning/wiki/Lightning-as-a-Base-Profile "Lightning sub-profile documentation"
[github]: https://github.com/acquia/lightning "GitHub clone"
[lightning_dev]: https://github.com/acquia/lightning-dev "Lightning Dev repository"
